#!/bin/bash

# Ejecutar las migraciones de la base de datos (Migracion hace referencia a que el proyecto de medusa configura la bd en postgres)
medusa migrations run

# Sembrar los datos, solo hace falta ejecutarlo una sola vez
# npx medusa seed --seed-file=data/seed.json
npx medusa seed --seed-file=seed.json

# medusa develop
# medusa start
medusa start

# Si quiera usar medusa admin desde aca simplemente iria a http://localhost:9000/app/login
# Usuario admin@medusa-test.com and password: supersecret