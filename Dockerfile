# Utilizar una imagen base de Node.js
FROM node:21.6.1

# Establecer el directorio de trabajo dentro del contenedor
WORKDIR /app

# Instalar Medusa CLI globalmente
RUN npm install -g @medusajs/medusa-cli@1.3.22 -y
RUN npm install medusa-payment-paypal
RUN npm install medusa-payment-stripe
RUN npm install @minskylab/medusa-payment-mercadopago

# Inicializar un proyecto Medusa
# RUN Copio el proyecto de medusa al directorio actual
WORKDIR /app/local-medusa-server
COPY medusa-backend/* .

# Instalo el complemento de mercadopago
# RUN npm install @minskylab/medusa-payment-mercadopago

RUN rm -rf node_modules
RUN npm install -y

ENTRYPOINT [ "./entrypoint.sh" ]

#admin@medusa-test.com
#supersecret

